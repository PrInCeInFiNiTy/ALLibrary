//
//  ALAPIModel.swift
//  ALLibary
//
//  Created by PrInCeInFiNiTy on 7/24/18.
//  Copyright © 2018 PrInCeInFiNiTy. All rights reserved.
//

import UIKit
import Alamofire

public enum ALAPIRequest: URLRequestConvertible {
    static let baseURL = ""
    
    case M_radioList()
    case M_Device(vender_id:String, device_os:String, notification_token:String)

    // MARK: - HTTPMethod
    private var method: HTTPMethod {
        switch self {
        case .M_radioList:
            return .post
        case .M_Device:
            return .post
        }
    }
    
    // MARK: - Path
    private var path: String {
        switch self {
        case .M_radioList:
            return "service/RadioPlayList.php"
        case .M_Device:
            return "service/DeviceProfile.php"
        }
    }
    
    // MARK: - Parameters
    private var parameters: Parameters? {
        switch self {
        case .M_radioList:
            return [:]
        case .M_Device(let vender_id, let device_os, let notification_token):
            return ["vender_id":vender_id, "device_os":device_os, "notification_token":notification_token]
        }
    }
    
    // MARK: - HeaderField
    enum HTTPHeaderField: String {
        case authentication = "Authorization"
        case contentType = "Content-Type"
        case acceptType = "Accept"
        case acceptEncoding = "Accept-Encoding"
    }
    
    // MARK: - ContentType
    enum ContentType: String {
        case json = "application/json"
    }
    
    // MARK: - URLRequestConvertible
    public func asURLRequest() throws -> URLRequest {
        let url = try ALAPIRequest.baseURL.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        
        // HTTP Method
        urlRequest.httpMethod = method.rawValue
        
        // Common Headers
        urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.acceptType.rawValue)
        urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.contentType.rawValue)
        
        // Parameters
        if let parameters = parameters {
            do {
                urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
            } catch {
                throw AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
            }
        }
        
        return urlRequest
    }
}

