//
//  ALClientRequest.swift
//  ALLibary
//
//  Created by PrInCeInFiNiTy on 7/26/18.
//  Copyright © 2018 PrInCeInFiNiTy. All rights reserved.
//

import Foundation
import Alamofire

class ALCenterRequest: NSObject {
    static func request(requestRount:URLRequestConvertible, sucsess: @escaping(Any)->Void, fail: @escaping(Any)->Void)
    {
        Alamofire.request(requestRount).responseJSON { (response) in
            switch response.result {
            case .success(let value):
                sucsess(value)
            case .failure(let error):
                fail(error.localizedDescription)
            }
        }
    }
}
