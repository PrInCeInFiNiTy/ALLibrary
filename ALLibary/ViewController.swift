//
//  ViewController.swift
//  ALLibary
//
//  Created by PrInCeInFiNiTy on 7/22/18.
//  Copyright © 2018 PrInCeInFiNiTy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var viewColor: UIView!
    let shapeLayer = CAShapeLayer()
    let trackLayer = CAShapeLayer()
    var click:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        print(ALString.removeNullString(stringData: nil))
        
        print("=============================")
        
        print(ALString.changeNullString(stringData: nil, changeString: "OK"))
        
        print("=============================")
        
        print(ALString.changeNullString(stringData: "i'm Apple", changeString: "OK"))
        
        print("=============================")
        
        print(ALString.emailValid(stringData: "p.light3213@gmail.com"))
        
        print("=============================")
        
        print(ALString.emailValid(stringData: "p.light@gmailcom"))
        
        print("=============================")
        
        print(ALString.stringOf2Decimal(stringData: "123213123123.54"))
        
        print("=============================")
        
//        self.viewColor.backgroundColor = UIColor.colorWithHexColor(strHex: "##FD9564", alpha: 1)
        
        print(ALString.stringWithViewName(viewName: "Home", keyName: "Login", stringIsNull: "NoData"))

        let requestRadioList = ALAPIRequest.M_radioList()
        ALCenterRequest.request(requestRount: requestRadioList, sucsess: { (datDic) in
            print(datDic)
        }) { (datDic) in
            print(datDic)
        }
        
        let requestDevice = ALAPIRequest.M_Device(vender_id: "abassdfgg", device_os: "ios", notification_token: "asd12345")
        ALCenterRequest.request(requestRount: requestDevice, sucsess: { (datDic) in
            print(datDic)
        }) { (datDic) in
            print(datDic)
        }

        let array:NSMutableArray = ALDataSource.dicJsonData["uploadPost"] as Any as! NSMutableArray
        print(array.count)
        
        print("=============================")
        
        let arrayAdddic:NSMutableArray = ALDataSource.dicJsonData["uploadPost"] as! NSMutableArray
        print(arrayAdddic)
        let dicData:Dictionary = ["postname":"Drink","description":"Wow"]
        arrayAdddic.add(dicData)
        print(arrayAdddic)
        print(ALDataSource.dicJsonData)
        ALDataSource.upDateJson()
        
        print("=============================")
        
        ALDataSource.reLoadDataJson()
        let arrayReload:NSMutableArray = ALDataSource.dicJsonData["uploadPost"] as Any as! NSMutableArray
        print(arrayReload)

        print(ALDataSource.getlocaltime())
        
        
        // ProgressCircular
        let center = view.center
        let circularPath = UIBezierPath(arcCenter: center, radius: 100, startAngle: -CGFloat.pi/2, endAngle: 2 * CGFloat.pi, clockwise: true)
        trackLayer.path = circularPath.cgPath
        trackLayer.strokeColor = UIColor.lightGray.cgColor
        trackLayer.fillColor = UIColor.clear.cgColor
        trackLayer.lineWidth = 20
        trackLayer.lineCap = kCALineCapRound
        view.layer.addSublayer(trackLayer)
        shapeLayer.path = circularPath.cgPath
        shapeLayer.strokeColor = UIColor.red.cgColor
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.lineWidth = 20
        shapeLayer.strokeEnd = 0
        shapeLayer.lineCap = kCALineCapRound
        view.layer.addSublayer(shapeLayer)
        
    }
    
 
   
    @IBAction func butActionProgress(_ sender: Any) {
        let baseAnimation = CABasicAnimation(keyPath: "strokeEnd")
        if click == 0 {
            baseAnimation.toValue = 0.3
            baseAnimation.fillMode = kCAFillModeForwards
            click = 1
        }
        else
        {
            shapeLayer.strokeEnd = 0.3
            baseAnimation.toValue = 0.1
            baseAnimation.fillMode = kCAFillModeBackwards
            click = 0
            shapeLayer.strokeEnd = 0.1
        }
//        baseAnimation.duration = 1
        baseAnimation.isRemovedOnCompletion = false
        
        shapeLayer.add(baseAnimation, forKey: "urSoBasic")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

