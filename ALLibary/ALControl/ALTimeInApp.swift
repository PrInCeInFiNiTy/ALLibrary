//
//  ALTimeInApp.swift
//  ALLibary
//
//  Created by ice on 30/7/2561 BE.
//  Copyright © 2561 PrInCeInFiNiTy. All rights reserved.
//

import Foundation

extension ALDataSource
{
    static func getlocaltime() -> String
    {
        var localTimeZone:String { return TimeZone.current.abbreviation() ?? "" }
        return localTimeZone
    }
}
