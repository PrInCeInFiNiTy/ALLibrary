//
//  ALJsonReadWriteFile.swift
//  ALLibary
//
//  Created by ice on 28/7/2561 BE.
//  Copyright © 2561 PrInCeInFiNiTy. All rights reserved.
//

import UIKit

extension ALDataSource {
    
    static func openfileJson(stringFileName:String?){
        if ALString.removeNullString(stringData: stringFileName) != ""
        {
            if let pathJson = Bundle.main.path(forResource: stringFileName, ofType: "json")
            {
                do
                {
                    let data = try Data(contentsOf: URL(fileURLWithPath: pathJson), options: .alwaysMapped)
                    let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                    dicJsonData = (jsonResult as? [String:AnyObject])!
                }
                catch
                {
                    dicJsonData = [:]
                }
            }
            else
            {
                dicJsonData = [:]
            }
        }
    }
    
    static func reLoadDataJson() {
      
            if let pathJson = Bundle.main.path(forResource: jsonFileName, ofType: "json")
            {
                do
                {
                    let data = try Data(contentsOf: URL(fileURLWithPath: pathJson), options: .alwaysMapped)
                    let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                    dicJsonData = (jsonResult as? [String:AnyObject])!
                }
                catch
                {
                    dicJsonData = [:]
                }
            }
            else
            {
                dicJsonData = [:]
            }
    }
    
    static func upDateJson() {
        let pathJson = Bundle.main.path(forResource: jsonFileName, ofType: "json")
        let fileWriteFileHandle = FileHandle(forWritingAtPath: pathJson!)
        if fileWriteFileHandle != nil {
            do{
                if let jsonData = try JSONSerialization.data(withJSONObject: dicJsonData, options: .prettyPrinted) as? Data
                {
                    print(NSString(data: jsonData, encoding: 1)!)
                    fileWriteFileHandle?.write(jsonData)
                }
            }
            catch {
                
            }
            fileWriteFileHandle?.closeFile()
        }
        else {
            print("Something went wrong!")
        }
    }
    
}
