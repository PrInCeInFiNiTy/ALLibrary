//
//  ALString.swift
//  ALLibary
//
//  Created by PrInCeInFiNiTy on 7/22/18.
//  Copyright © 2018 PrInCeInFiNiTy. All rights reserved.
//

import UIKit

class ALString: NSObject {
    
   static var dicDataLanguage = NSDictionary()
    
    static func removeNullString(stringData: String?) -> String
    {
        if (stringData?.isEmpty) != nil
        {
            return String(format: "%@", stringData!)
        }
        else
        {
            return("")
        }
    }
    
    static func changeNullString(stringData: String? , changeString: String?) -> String
    {
        if (stringData?.isEmpty) != nil && (changeString?.isEmpty) != nil
        {
            return String(format: "%@", stringData!)
        }
        else if (changeString?.isEmpty) != nil
        {
            return String(format: "%@", changeString!)
        }
        else
        {
            return("")
        }
    }
    
    static func emailValid(stringData: String?) -> Bool
    {
        if removeNullString(stringData: stringData) != ""
        {
            let emailFormat:String! = "^[_a-z0-9-]+(\\.[_a-z0-9-]+)*@[a-z0-9-]+(\\.[a-z0-9-]+)*(\\.[a-z]{2,20})$"
            let predicateEmailFormat = NSPredicate(format:"SELF MATCHES %@", emailFormat)
            return predicateEmailFormat.evaluate(with: stringData)
        }
        else
        {
            return false
        }
    }
    
    static func stringOf2Decimal(stringData: String?) -> String
    {
        if removeNullString(stringData: stringData) != ""
        {
            let numberDouble:Double = Double(stringData!) ?? 0.00
            let numberFormat = NumberFormatter()
            numberFormat.locale = NSLocale(localeIdentifier: "th_TH") as Locale?
            numberFormat.minimumFractionDigits = 2
            numberFormat.maximumFractionDigits = 2
            numberFormat.decimalSeparator = "."
            numberFormat.positiveFormat = "###,###,###.00"
            return numberFormat.string(from: NSNumber(value: numberDouble))!
        }
        else
        {
            return "0.00"
        }
    }
    
    static func stringWithViewName(viewName:String? , keyName:String? , stringIsNull:String?) -> String
    {
        if removeNullString(stringData: viewName) != "" && removeNullString(stringData: keyName) != ""
        {
            var dicDataScreen = NSDictionary()
            var dicDataValue = NSDictionary()
            if dicDataLanguage.count != 0 {
                dicDataScreen = dicDataLanguage.object(forKey: viewName!) as! NSDictionary
                dicDataValue = dicDataScreen.object(forKey: keyName!) as! NSDictionary
                return dicDataValue.object(forKey: ALDataSource.deviceLanguage) as! String
            } else {
                dicDataLanguage = ALPlistManager.openFilePlist(stringFileName: ALDataSource.deviceLanguageFileName)
                if dicDataLanguage.count > 0
                {
                     dicDataScreen = dicDataLanguage.object(forKey: viewName!) as! NSDictionary
                     dicDataValue = dicDataScreen.object(forKey: keyName!) as! NSDictionary
                     return dicDataValue.object(forKey: ALDataSource.deviceLanguage) as! String
                }
                else
                {
                     return removeNullString(stringData: stringIsNull)
                }
            }
        }
        else
        {
            return removeNullString(stringData: stringIsNull)
        }
    }
    
}


