//
//  ALPlistManager.swift
//  ALLibary
//
//  Created by PrInCeInFiNiTy on 7/22/18.
//  Copyright © 2018 PrInCeInFiNiTy. All rights reserved.
//

import UIKit

class ALPlistManager: NSObject {
    
    static func openFilePlist(stringFileName:String?) -> NSDictionary
    {
        var dicDataRetune = NSDictionary()
        if ALString.removeNullString(stringData: stringFileName) != ""
        {
            let path = Bundle.main.path(forResource: stringFileName, ofType: "plist")
            if path != nil
            {
                dicDataRetune = NSDictionary(contentsOfFile: path!)!
                return dicDataRetune
            }
            else
            {
                return dicDataRetune
            }
        }
        else
        {
            return dicDataRetune
        }
    }
    
}
