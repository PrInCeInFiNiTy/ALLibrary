//
//  ALHexColor.swift
//  ALLibary
//
//  Created by PrInCeInFiNiTy on 7/22/18.
//  Copyright © 2018 PrInCeInFiNiTy. All rights reserved.
//

import UIKit

extension UIColor {
    
    static func colorWithHexColor(strHex:String , alpha:CGFloat) -> UIColor {
    let strHexColor:String = strHex.replacingOccurrences(of: "#", with: "")
    var rgbValue:UInt32 = 0
    Scanner(string: strHexColor).scanHexInt32(&rgbValue)
    return UIColor.init(red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
                        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0 ,
                        blue: CGFloat(rgbValue & 0x0000FF) / 255.0 ,
                        alpha: alpha)
    }
    
}
