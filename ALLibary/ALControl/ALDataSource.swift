//
//  ALDataSource.swift
//  ALLibary
//
//  Created by PrInCeInFiNiTy on 7/22/18.
//  Copyright © 2018 PrInCeInFiNiTy. All rights reserved.
//

import UIKit

class ALDataSource: NSObject {

    static var appVersion = String()
    static var userToken = [Any]()
    static var userNotificationToken = String()
    static let deviceLanguageFileName:String = "language"
    static var deviceLanguage:String = "EN"
    static let jsonFileName:String = "jsonDataUpLoad"
    static var dicJsonData = [String:Any]()
   
    static func initFunctionALLibrary()
    {
        let defaultsStore = UserDefaults.standard
        if (defaultsStore.object(forKey: "language") != nil) {
            self.deviceLanguage = defaultsStore.object(forKey: "language") as! String
        }
        else
        {
            defaultsStore.set(self.deviceLanguage, forKey: "language")
        }
        
        ALDataSource.openfileJson(stringFileName: jsonFileName);
    }
    
}
